terraform {
  backend "s3" {
    bucket = "tf-state-study"
    key    = "docker-fargate.terraform.tfstate"
    region = "eu-west-1"
  }
}