# variables.tf

variable "aws_region" {
  description = "The AWS region things are created in"
  default     = "ap-northeast-2"
}

variable "ecs_task_execution_role_name" {
  description = "ECS task execution role name"
  default = "myEcsTaskExecutionRole"
}

variable "az_count" {
  description = "Number of AZs to cover in a given region"
  default     = "2"
}

variable "app_image" {
  description = "Docker image to run in the ECS cluster"
  default     = "sunny5/bulletinboard:1.2" // 변경 필요 (본인의 이미지 in Docker Hub, 만약 다른 앱 소스를 사용한다면)
}

variable "app_port" {
  description = "Port exposed by the docker image to redirect traffic to"
  default     = 8080 // 변경 필요 (본인 App의 출력 포트, 만약 다른 앱 소스를 사용한다면)
}

variable "app_count" {
  description = "Number of docker containers to run"
  default     = 3
}

variable "health_check_path" {
  default = "/"
}

variable "fargate_cpu" {
  description = "Fargate instance CPU units to provision (1 vCPU = 1024 CPU units)"
  default     = "1024"
}

variable "fargate_memory" {
  description = "Fargate instance memory to provision (in MiB)"
  default     = "2048"
}

